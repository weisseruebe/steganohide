
'''
Created on 02.12.2010

@author: andreasrettig
'''
from PIL import Image

def getNthByte(n, im):
    pix = im.load()
    width, height = im.size
    return pix[n/3 % width, n/3 / width][n % 3]
  
def setLastBit(source, dataBit):
    return (source & 0xFE) | (dataBit & 0x01)

def setNthBytes(n, im, bytes):
    pix = im.load()

    width, height = im.size
    pixelNr = n/3
    line    = pixelNr / width
    column  = pixelNr % width
    picPos  = n % 3;
    if (picPos == 0):
        pix[column, line] = (setLastBit(pix[column, line][0],bytes), pix[column, line][1], pix[column, line][2]) 
    elif (picPos == 1):
        pix[column, line] = (pix[column, line][0],setLastBit(pix[column, line][1],bytes),pix[column, line][2]) 
    elif (picPos == 2):
        pix[column, line] = (pix[column, line][0],pix[column, line][1],setLastBit(pix[column, line][2],bytes)) 
  
def embed(data, filename):
    picPos = 0
    im = Image.open(filename)
    bitsPerPixel = 8
    for byte in data:
        for i in range(bitsPerPixel):
            byte = byte & 0xff
            setNthBytes(bitsPerPixel-1-i+picPos,im,byte)
            byte = byte >> 1
        picPos += bitsPerPixel
    return im

def recover(img, length):
    picPos  = 0
    for c in range(length):
        byte = 0
        for i in range(8):
            byte  = byte << 1
            byte |=  getNthByte(picPos,img) & 0x01
            picPos += 1
        yield byte
       
   
def main(): 
    string = "Traum Trecker Companie bietet Ihnen die Moglichkeit historische Schlepper fur einen oder mehrere Tage zu mieten und die Gegend zwischen Luneburg und Schnackenburg zu ertreckern. Fur mehrtagige Reisen empfehlen wir Ihnen unsere Bauwagen, in denen Sie auch ubernachten konnen." 
    img = embed(map(ord,string))
    print (bytearray(recover(img,len(string))))
    
if __name__ == '__main__':
    main()

