""" 
XTEA Block Encryption Algorithm

Original Code: http://code.activestate.com/recipes/496737/
Algorithm:     http://www.cix.co.uk/~klockstone/xtea.pdf 

""" 

TEA_BLOCK_SIZE = 8
TEA_KEY_SIZE = 16
mask  = 0xffffffff

def key_blocks(key):
    return [ (key >> (32 * i)) & mask for i in range(3, -1, -1) ]

def byte_sequence(block, n=8):
    for i in range(n - 1, -1, -1):
        yield block >> i * 8 & 0xff

def encrypt(key, iv, plain):
    feedback = iv
    i, block = 0, 0
    for p in plain:
        block = block << 8 | p
        i += 1
        if i == 8:
            feedback = block ^ xtea_encrypt(key, feedback)
            for c in byte_sequence(feedback):
                yield c
            i, block = 0, 0
    for c in byte_sequence(block ^ (xtea_encrypt(key, feedback) ), i):
        yield c
        
def decrypt(key, iv, crypt):
    feedback = iv
    i, block = 0, 0
    for c in crypt:
        block = block << 8 | c
        i += 1
        if i == 8:
            for p in byte_sequence(block ^ xtea_encrypt(key, feedback)):
                yield p
            feedback = block
            i, block = 0, 0
    
    for p in byte_sequence(block ^ (xtea_encrypt(key, feedback)), i):
        yield p

def xtea_encrypt(key,block,n=64):
        delta = 0x9e3779b9
        v0 = block >> 32
        v1 = block & mask
        k =  key_blocks(key)
        sum = 0
        for round in range(n):
            v0 = (v0 + (((v1<<4 ^ v1>>5) + v1) ^ (sum + k[sum & 3]))) & mask
            sum = (sum + delta) & mask
            v1 = (v1 + (((v0<<4 ^ v0>>5) + v0) ^ (sum + k[sum>>11 & 3]))) & mask
        return v0 << 32 | v1

def xtea_decrypt(key,block,n=64):
        delta = 0x9e3779b9
        v0 = block >> 32
        v1 = block & mask
        k =  key_blocks(key)
        sum = (delta * n) & mask
        for round in range(n):
            v1 = (v1 - (((v0<<4 ^ v0>>5) + v0) ^ (sum + k[sum>>11 & 3]))) & mask
            sum = (sum - delta) & mask
            v0 = (v0 - (((v1<<4 ^ v1>>5) + v1) ^ (sum + k[sum & 3]))) & mask
        return v0 << 32 | v1

