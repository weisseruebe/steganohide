'''
Created on 26.04.2012

@author: andreasrettig
'''

from hashlib import sha256
class HMAC:
    
    def __init__(self, key, msg):
        self.outer = sha256()
        self.inner = sha256()
        self.digest_size = self.inner.digest_size
        blocksize = 64
        if len(key) > blocksize:
            key = sha256(key).digest()
        key = key + chr(0) * (blocksize - len(key))
        trans_5C = "".join ([chr (x ^ 0x5c) for x in xrange(256)])
        trans_36 = "".join ([chr (x ^ 0x36) for x in xrange(256)])
        self.outer.update(key.translate(trans_5C))
        self.inner.update(key.translate(trans_36))
        if msg:
            self.inner.update(msg)
        self.h = self.outer.copy()
        self.h.update(self.inner.digest())
 
    def hexdigest(self):
        return self.h.hexdigest()