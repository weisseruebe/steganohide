'''
Created on 15.05.2012

@author: andreasrettig
'''
from PIL import Image
import argparse
from hmac import HMAC
from itertools import chain
from xtea import encrypt, decrypt
import hashlib
import stegano
import random

def pack(b):
    return reduce(lambda x, y: (x<<8) + y, b)

def unpack(l, n):
    for i in range(n - 1, -1, -1):
        yield (l >> 8 * i) & 0xff
        
def hide(message, iv, macKey, cryptKey, sourceFilename):
    mac = bytearray(HMAC(macKey, message).hexdigest().decode("hex"))
    size = unpack(len(message),2)
    
    encrypted = encrypt(cryptKey, iv, chain(mac,message))
    return stegano.embed(chain(size,unpack(iv,8),encrypted), sourceFilename)  
     
def recover(image, key, macKey):
    # First retrieve Size and IV
    header = bytearray(stegano.recover(image, 10))
    size = pack(header[:2])
    ivp =  pack(header[2:10])
    
    enc = bytearray(stegano.recover(image, size+42)) #42 is headersize and MAC
    decrypted = bytearray(decrypt(key, ivp, enc[10:]))
    dMac  = pack(decrypted[:32])
    dData = decrypted[32:]
  
    macFromData = int(HMAC(macKey, dData).hexdigest(),16)
    if (dMac == macFromData):
        print "Decrypted message:"
        print str(dData)
    else:
        print "Message integrity is wrong!"
    
def main():
    parser = argparse.ArgumentParser(description='Embed a Text File into a pixel graphic.')
    parser.add_argument('-d', '--decrypt', action='store_true', help='Decrypt a message')
    parser.add_argument('-e', '--encrypt', action='store_true', help='Encrypt a message')

    parser.add_argument('-k', '--password', type=str, required=True, metavar='<password>', help='The password used for encryption')
    parser.add_argument('-m', '--macpassword', type=str, required=True, metavar='<macpassword>', help='The password used for integrity checking')
    parser.add_argument('files', nargs=argparse.REMAINDER)
      
    args = parser.parse_args()
   
    xteaKey = args.password
    macKey = args.macpassword
       
    hashedKey = int(hashlib.sha256(xteaKey).hexdigest(), 16) >> 128
   
    if args.decrypt:
        print "Decryption"
        imageFileName = args.files[0]

        imageFile = Image.open(imageFileName)
        recover(imageFile, hashedKey, macKey)

    if args.encrypt:
        if (len(args.files)) < 2:
            raise Exception("Needs two filenames")
        
        contentFileName = args.files[0]
        message = file(contentFileName).read()
        imageFileName = args.files[1]
        
        iv = random.randint(0, 0xffffffffffffffff)
    
        image = hide(bytearray(message), iv, macKey, hashedKey, imageFileName)
        image.save("steg"+imageFileName)
        print "Encrypted "+contentFileName+" into steg"+imageFileName
    
if __name__ == '__main__':
    main()
    